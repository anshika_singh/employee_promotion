In this project I have built a machine learning models to classify whether an employee should be promoted or not. I have analysed 
what are the important attributes that leads to an employee's promotion. For this project following models were used :
Logistic Regression, Random Forest, Naive Bayes and Gradient Boosting Methods.